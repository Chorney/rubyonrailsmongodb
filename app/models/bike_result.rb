
class BikeResult < LegResult

  field :mph, type: Float

  def calc_ave
    if self.event && self.secs
      mph = self.event.miles/(self.secs/3600)
      self.mph=mph.nil? ? nil : mph
    end
  end

end
