
class RunResult < LegResult

  field :mmile, as: :minute_mile, type: Float

  def calc_ave
    if self.event && self.secs
      minute_mile = ((self.secs/60.0)/self.event.miles)
      self.minute_mile=minute_mile.nil? ? nil : minute_mile
    end
  end

end
