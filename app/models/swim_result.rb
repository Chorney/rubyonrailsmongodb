
class SwimResult < LegResult

  field :pace_100, type: Float

  def calc_ave
    if self.event && self.secs
      meters = self.event.meters
      avg_time = (self.secs/meters)*100 #time to travel 100m
      self.pace_100=avg_time.nil? ? nil : avg_time
    end
  end

end
