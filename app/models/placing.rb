
class Placing

  attr_accessor :name, :place

  def initialize(place,name=nil)
    @name = name #params[:name] ||= params["name"]
    @place = place #params[:place] ||= params["place"]
  end

  def mongoize
    #hash = {}
    #if @name!=nil
   #  hash[:name] = @name
    #end
    #if @place!=nil
    #  hash[:place] = @place
    #end
    #return hash
    @name ? {:name => @name, :place => @place} : {:place => @place}
  end

  def self.demongoize(object)
    case object
    when Hash then Placing.new(object[:place],object[:name])
    else nil
    end
    #if params==nil
  #  return nil
    #elsif params.class == Hash
      #puts params
      #puts params[:name]
      #place = params[:place]
      #puts name
    #  return Placing.new(params)
    #elsif params.class == Placing
    #  return params
    #end
  end

  def self.mongoize(object)
    case object
    when Placing then object.mongoize
    else object
    end
    #if input==nil
    #  return nil
    #elsif input.class == Placing
    #  return input.mongoize
    #elsif input.class == Hash
    #  return input
    #end
  end

  def self.evolve(object)
    case object
    when Placing then object.mongoize
    else object
    end
    #if input == nil
    #  return nil
    #elsif input.class == Placing
    #  return input.mongoize
    #elsif input.class == Hash
    #  return input
    #end
  end
end
