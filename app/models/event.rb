class Event
  include Mongoid::Document
  field :o, as: :order, type: Integer
  field :n, as: :name, type: String
  field :d, as: :distance, type: Float
  field :u, as: :units, type: String

  embedded_in :parent, polymorphic: true, touch: true #touch as true makes timestamp updated for parent when event is modified
  validates_presence_of :order, :name
  

  def meters
    if self.units == "miles"
      return self.distance/0.000621371
    elsif self.units == "meters"
      return self.distance
    elsif self.units == "yards"
      return self.distance*0.9144
    elsif self.units =="kilometers"
      return self.distance*1000
    else
      return nil
    end
  end

  def miles
    if self.units =="miles"
      return self.distance
    elsif self.units == "meters"
      return self.distance*0.000621371
    elsif self.units == "yards"
      return self.distance*0.000568182
    elsif self.units == "kilometers"
      return self.distance*0.621371
    else
      return nil
    end
  end

end
