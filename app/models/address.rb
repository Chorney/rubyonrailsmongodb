
class Address

  attr_accessor :city, :state, :location


  def initialize(city=nil,state=nil,loc=nil)
    @city = city
    @state = state
    @location = Point.demongoize(loc)
  end

  def mongoize()
    out_hash = {}
    out_hash[:city]=@city
    out_hash[:state]=@state
    out_hash[:loc]= Point.mongoize(@location)
    return out_hash
  end

  def self.demongoize(object)

    #if params==nil
  #    return nil
  #  elsif params.class == Hash
  #    city = params[:city]
  #    state = params[:state]
  #    loc = params[:loc]
  #    return point = Address.new(:city=>city,:state=>state,:loc=>loc)
  #  elsif params.class == Address
  #    return params
  #  end

    case object
    when Hash then Address.new(object[:city],object[:state],object[:loc])
    else nil
    end

  end

  def self.mongoize(object)
    case object
    when Address then object.mongoize
    else object
    end
  end

  def self.evolve(object)
    case object
    when Address then object.mongoize
    else object
    end
  end


end
