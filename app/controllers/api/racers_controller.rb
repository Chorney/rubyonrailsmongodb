module Api
  class RacersController < ApplicationController

    def index
      #@races = Race.all.order_by(:date.desc)
      if !request.accept || request.accept == "*/*"
        render plain: "/api/racers"#{params[:id]}"
      else
        #real implementation ...
      end
    end

    # GET /races/1
    # GET /races/1.json
    def show
      #@entrants=Entrant.where(:"race._id"=>@race.id).order_by(:secs.asc,:last_name.asc,:first_name.asc)
      if !request.accept || request.accept == "*/*"
        render plain: "/api/racers/#{params[:id]}"
      else
        #real implementation ...
      end
    end

    def entries

      if !request.accept || request.accept == "*/*"
        render plain: "/api/racers/#{params[:racer_id]}/entries"#/#{params[:id]}"
      else
        #real implementation ...
      end
    end

    def entry

      if !request.accept || request.accept == "*/*"
        render plain: "/api/racers/#{params[:racer_id]}/entries/#{params[:id]}"
      else
        #real implementation ...
      end
    end



  end
end
