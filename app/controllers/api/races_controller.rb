module Api

  require 'builder'

  class RacesController < ApplicationController

    protect_from_forgery with: :null_session

    #ActiveSupport::CoreExtensions::Hash::Conversions::XML_PARSING.delete('yaml')

    rescue_from Mongoid::Errors::DocumentNotFound do |exception|
      #error comes here when there is no params[:id] in the database


      #if request.accept== "application/xml"
      #  render xml: "woops: cannot find race[#{params[:id]}]", status: :not_found
      #elsif request.accept=="application/json"
      #  render json: "woops: cannot find race[#{params[:id]}]", status: :not_found
      #else
      #  render plain: "woops: cannot find race[#{params[:id]}]", status: :not_found
      #end

      #if request.accept=="text/plain find race[#{params[:id]}]"
      if request.accept=="application/json" || request.accept=="application/xml"
        @msg = "whoops cannot find race[#{params[:id]}]"
        render action: :error, status: :not_found
      else
        render plain: "woops: cannot find race[#{params[:id]}]", status: :not_found
      end
      #else
      #  @msg = "fuck" #{}"whoops cannot find race[#{params[:id]}]"
      #  render action: :error, status: :not_found
      #end
    end

    #GET api/races
    def index
      #@races = Race.all.order_by(:date.desc)
      if !request.accept || request.accept == "*/*"

        render plain: "/api/races, offset=[#{params[:offset]}], limit=[#{params[:limit]}]"#{params[:id]}"#{}"/results/#{params[:id]}"
      else
        #real implementation ...
      end
    end

    # POST api/races/
    def create
      #puts params
      #puts params
      #puts params
      #puts params
      if !request.accept || request.accept == "*/*" #&& request.body == nil
        begin
          render plain: params[:race][:name], status: :ok
        rescue
          render plain: :nothing,  status: :ok
        end
      #elsif !request.accept
        #render plain: params[:race][:name], status: :ok
      else
        #render plain: :nothing, status: :ok
        #real implementation
        render plain: params[:race][:name], status: :created
        race = Race.create(:name=>params[:race][:name],:date=>params[:race][:date])
      end

      #if request.accept!= nil # || request.accept == "*/*"
      #  render plain: params[:race][:name], status: :created

      #  race = Race.create(:name=>params[:race][:name],:date=>params[:race][:date])
        #puts params[:race]
        #@race = Race.new(params[:race]_to_hash)
        #@race = Race.new(params[:race])
        #return params[:body][:race][:name]
        #render plain: :params[:body][:name][:name]
      #else
        #real implementation
      #end
    end

    # GET /races/1
    # GET /races/1.json
    def show
      #@entrants=Entrant.where(:"race._id"=>@race.id).order_by(:secs.asc,:last_name.asc,:first_name.asc)
      if !request.accept
        render plain: "/api/races/#{params[:id]}"#{}"/results/#{params[:id]}"
      elsif !request.accept.match("application/json") and !request.accept.match("application/xml")
        @msg = "woops: we do not support that content-type[#{request.accept}]"
        render plain: @msg, status: :unsupported_media_type
      else #request.accept == "application/xml"
        #real implementation ...
      #  race = Race.find_by(:id=>params[:id])
      #  @line1 = race.name
      #  @line2 = race.date
      #  render :race, content_type: "#{request.accept}"
      #elsif request.accept =="application/json"
      #  race = Race.find_by(:id=>params[:id])
      #  render json: race
      #race = Race.find(params[:id])
      #@msg = race.name
      #render :race, content_type: "#{request.accept}"
      #end

      #if !request.accept # || request.accept == "*/*"
      #  render plain: "/api/races/#{params[:id]}"#{}"/results/#{params[:id]}"


        @race = Race.find(params[:id])
      end
      #@msg = race.name
      #render xml: race
      #render :race, content_type: "#{request.accept}"
      #case request.accept
      #when "application/xml" then
      #  @msg = "fuck"
      #  render :race
      #when "application/json" then
      #  @msg = "fuck"
      #  render :race
      #else
      #  render plain: :nothing
      #end

    end

    # GET /api/races/:race_id/results
    def results

      if !request.accept || request.accept == "*/*"
        render plain: "/api/races/#{params[:race_id]}/results"#/#{params[:id]}"
      else
        #real implementation ...


        @race=Race.find(params[:race_id])
        #updated_at = @race.entrants.max(:updated_at)
        #@race.updated_at = updated_at
        #fresh_when last_modified: @race.entrants.max(:updated_at)

        if stale? last_modified: @race.entrants.max(:updated_at)
          @entrants=@race.entrants
        end

        #if headers["Last-Modified"] < @race.entrants.max(:updated_at)
        #if stale? @race.entrants.max(:updated_at)
        #  @entrants=@race.entrants
        #else
        #  render plain: :nothing
        #end
        #render plain: :nothing
        #render :plain :nothing
        #else
          #render :plain :nothing
        #end

        #render :plain header["Last-Modified"]

        #fresh_when last_modified:
        #if stale? @race
        #headers["Last-Modified"] = updated_at
        #@msg = @entr
        #will auto-call the results.json.jbuilder by convention



      end
    end

    def result
      if !request.accept || request.accept == "*/*"
        render plain: "/api/races/#{params[:race_id]}/results/#{params[:id]}"
      else
        #real implementation ...
        #render plain: "/api/races/#{params[:race_id]}/results/#{params[:id]}"
        @result=Race.find(params[:race_id]).entrants.where(:id=>params[:id]).first
        #render plain: @result.overall_place
        render :partial=>"result", :object=>@result
      end
    end


    #PATCH /api/races/:race_id/results/:id
    def fix


      #end
      entrant=Race.find(params[:race_id]).entrants.where(:id=>params[:id]).first
      #fresh_when(entrant)

      result=params[:result]

      #render plain: result[:swim]

      if result
        if result[:swim]
          entrant.swim=entrant.race.race.swim
          entrant.swim_secs = result[:swim].to_f
        end
        if result[:t1]
          entrant.t1=entrant.race.race.t1
          entrant.t1_secs = result[:t1].to_f
        end
        if result[:bike]
          entrant.bike=entrant.race.race.bike
          entrant.bike_secs = result[:bike].to_f
        end
        if result[:t2]
          entrant.t2=entrant.race.race.t2
          entrant.t2_secs = result[:t2].to_f
        end
        if result[:run]
          entrant.run=entrant.race.race.run
          entrant.run_secs=result[:run].to_f
        end
      end

      render plain: :nothing
      entrant.save
    end

    # PUT api/races/:id
    def update
      #if request.accept!= nil # || request.accept == "*/*"
        #render plain: "/api/races/#{params[:id]}"#{}"/results/#{params[:id]}"
        Rails.logger.debug("method=#{request.method}")

        race = Race.find(params[:id])
        race.update(:name=>params[:race][:name],:date=>params[:race][:date])

        render json: race

        #race = Race.update(:name=>params[:race][:name],:date=>params[:race][:date])
        #puts params[:race]
        #@race = Race.new(params[:race]_to_hash)
        #@race = Race.new(params[:race])
        #return params[:body][:race][:name]
        #render plain: :params[:body][:name][:name]
      #else
        #real implementation
        #render plain: "/api/races/#{params[:id]}"#{}"/results/#{params[:id]}"
      #end
    end

    #DELETE api/races/:id
    def destroy

      race = Race.find(params[:id])
      race.destroy
      render :nothing=>true, :status => :no_content

    end

  end
end
